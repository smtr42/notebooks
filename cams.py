import numpy as np
import xarray as xr
from datetime import datetime


def prepare_data(ds):
    prepare_longitude(ds)
    prepare_time(ds)
    
    
def prepare_longitude(ds):
    # Example https://github.com/pydata/xarray/issues/577#issuecomment-140457546
    west_longitude = ds.longitude[ds.longitude >= 180] - 360
    east_longitudes = ds.longitude[ds.longitude < 180]
    normalized_longitudes = xr.concat([west_longitude, east_longitudes], dim="longitude")

    ds.coords["longitude"] = normalized_longitudes
    ds.reindex(longitude=normalized_longitudes);


def prepare_time(ds):
    start_date = read_start_date(ds)
    normalized_time = ds.time + start_date
    ds.coords["time"] = normalized_time
    ds.reindex(time=normalized_time);
    
    
def read_start_date(ds):
    dt = datetime.strptime(ds.FORECAST, r"Europe, %Y%m%d+[0H_24H]")
    start_date = np.datetime64(dt)
    return start_date