import pandas as pd


def read_observations(stations, filename):
    observations = pd.read_csv(
        filename, na_values="mq",
        delimiter=";", parse_dates=['date'],
        index_col='date'
    )
    df = observations.join(stations, on="numer_sta")
    df = df[["Nom", "t", "Latitude", "Longitude"]]
    df['temperature'] = df['t'] - 273.15
    del df['t']
    return df


def read_stations(filename="data/csv/postesSynop.csv"):
    return pd.read_csv(filename, delimiter=";", index_col="ID")